class Theme:

    def __init__(self, colors: list, theme_template: str):
        self.colors = colors
        self.theme_template = theme_template


    def get_theme_with_applied_colors(self) -> str:
        theme = self.theme_template
        for color in self.colors:
            theme = theme.replace(color.key, color.value)

        return theme


class Color:

    def __init__(self, key: str, value: str):
        self.key = key
        self.value = value
