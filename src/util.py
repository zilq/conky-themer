import optparse
import theme

PARSER_USAGE = "conky_themer -t <theme> -cs <colorscheme>"

def get_parser() -> optparse.OptionParser:
    parser = optparse.OptionParser(PARSER_USAGE)
    parser.add_option("--theme", dest="theme", type="string", help="specifiy theme")
    parser.add_option("--colorscheme", dest="colorscheme", type="string", help="specify colorscheme")

    return parser


def get_theme(theme_name: str, colorscheme: str) -> theme.Theme:
    colors = get_colors(colorscheme)
    theme_template = get_theme_template(theme_name)

    return theme.Theme(colors, theme_template)


def get_colors(colorscheme: str) -> list:
    colorscheme_file_contents = get_file_contents(colorscheme).splitlines()
    colors = map(lambda file_line: createColorItem(file_line), colorscheme_file_contents)
    return list(colors)
    

def createColorItem(input: str) -> theme.Color:
    key_value_pair = input.split('=', 1)
    key = key_value_pair[0].strip()
    value = key_value_pair[1].strip()
    return theme.Color(key, value)


def get_theme_template(theme: str) -> str:
    return get_file_contents(theme)


def get_config() -> str:
    return get_file_contents("config")


def get_file_contents(filename: str) -> str:
    try:
        with open(filename) as f:
            return f.read()
    except FileNotFoundError:
        print("File {0} not found", filename)