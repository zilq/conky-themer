#!/usr/bin/env python3

import util
import getpass
import theme

def main():
   (theme, colorscheme) = get_arguments()
   theme = util.get_theme(theme, colorscheme)
   apply_theme(theme)

def get_arguments():
    parser = util.get_parser()
    (options, args) = parser.parse_args()
    if (options.theme is None) or (options.colorscheme is None):
        print(util.PARSER_USAGE)
        print("Exiting")
        exit(-1)

    return (options.theme, options.colorscheme)


def apply_theme(theme: theme.Theme):
    new_config = create_new_config_with_theme(theme)
    save_config_to_file(new_config)


def create_new_config_with_theme(theme: theme.Theme) -> str:
    theme_with_colors = theme.get_theme_with_applied_colors()
    config = util.get_config()

    return config + "\n\n" + theme_with_colors


def save_config_to_file(config: str):
    user = getpass.getuser()
    file_path = "/home/{0}/.config/conky/conky.conf".format(user)

    try:
        with open(file_path, "w") as config_file:
            for line in config:
                config_file.write(line)
    except FileNotFoundError:
        print("File {0} not found", filename)


if __name__ == "__main__":
    main()
